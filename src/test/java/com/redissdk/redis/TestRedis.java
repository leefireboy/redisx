package com.redissdk.redis;

import com.redissdk.redis.PowerfulRedisUtil;

/**
  * 项目名称:[redisx]
  * 包:[com.redissdk.redis]    
  * 文件名称:[TestRedis]  
  * 描述:[一句话描述该文件的作用]
  * 创建人:[彭小林]
  * 创建时间:[2017年1月3日 上午11:43:58]   
  * 修改人:[彭小林]   
  * 修改时间:[2017年1月3日 上午11:43:58]   
  * 修改备注:[说明本次修改内容]  
  * 版权所有:luwenbin006@163.com
  * 版本:[v1.0]
 */
public class TestRedis
{
   public static void main(String[] args)
{
       
       for(int i=0;i<500;i++)
       {
           new RedisThread("threadredis"+i).start();

       }
}
}

class RedisThread extends Thread
{
  private String name;
  
  public RedisThread()
{
    // TODO Auto-generated constructor stub
}
  public RedisThread(String name)
  {
      this.name = name;
  }

@Override
  public void run()
  {
      for(int i=0;i<50;i++)
      {
          System.out.println(PowerfulRedisUtil.setString(this.name+"xxx"+i, "fdsfsdf"+i)+"="+i+"="+this.getName()+this.getId());

      }
      
      for(int i=0;i<50;i++)
      {
          System.out.println(PowerfulRedisUtil.del(this.name+"xxx"+i));

      }
  }
}
